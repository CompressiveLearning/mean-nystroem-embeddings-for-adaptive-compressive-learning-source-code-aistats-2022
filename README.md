# Info

This repository contains the code to generate the CSV results of the paper "Mean Nyström Embeddings for Adaptive Compressive Learning", accepted to AISTATS 2022.

It contains only wrapper functions and the choice of parameters used for the experiments. The implementation of the Nyström features itself is part of the CompressiveLearning package (from version 0.2.0), which is included as a dependency.

# Installation

Clone the repository and instantiate it by running `julia --project=.` from this directory and then `]instantiate` from the julia REPL.
To execute the code computing leverage scores, you will need additional python dependencies. Please follow the instructions given [here](https://gitlab.com/CompressiveLearning/CompressiveLearning.jl#installation).
Note that these dependencies should be installed for the python version used by PyCall. 

# Reproduce

The source files are contained in the `src/` repository, and can be run one by one using the `include` command, or all at once via the `main.jl` file (this should take quite some time). 

All experiments are run with ExperimentsManager, which supports parallelization with multiple processes or threads. Depending on your configuration, you might want to adapt the parameters provided to `ExperimentsManager.run` in consequence.

# Results

By default, the results will be stored in the `results_LocalExpBE` dirctory. Each experiment corresponds to a subdirectory, with a `logs` folder containing textual logs and a `logdict` folder containing results in CSV format.

The code to produce the figures is not included, but essentially amounts to reading the produced CSV files, grouping by method/feature types and plotting.

