use_threads = false
using Distributed, Base.Threads
# ~use_threads && addprocs(max(Int(Sys.CPU_THREADS/2)-nprocs(), 0); )
# @everywhere ENV["DATADEPS_LOAD_PATH"] = fetch(@spawnat 1 ENV["DATADEPS_LOAD_PATH"])
@everywhere using Pkg
@everywhere Pkg.activate(dirname(@__DIR__))
@everywhere using CompressiveLearning
@everywhere using ExperimentsManager
@everywhere using FatDatasets
@everywhere using PyCall
@info "Using $(nprocs()) procs and $(nthreads()) threads."

runlocally()

k = 10
σ² = 5000
gridres_m = 15

e = ExperimentsManager.Exp(; 
						   name = "gmm_fma_mfcc_k$(k)_var$(σ²)", 
						   iterations = 50)
dict = logdict(e)
e.workflow = [
	(:X, [FatDatasets.load_dataset => PrmSpace([
			:name => ["fma_mfcc"]
		]),
	]),
	(:C, [
		CGMM => PrmSpace([
		 	:X => [WorkflowResult(:X)],
		 	:k => [k],
		], KwArgs(
			:approximation_type => [:Nyström => KwArgs(
										:Nyström_sampling_type => [
											:uniform,
											:greedy_leverage_scores, 
										],  
										:linop_type => [:dense]
									), 
									:random_features, 
								],
			:m_factor_eq_reals => 10 .^range(log10(2.0), stop=log10(100), length = gridres_m),
			:kernel_var => [σ²], 
			:kernel_type => [:Gaussian],
			:decoder => [:CLOMPR],
			:out_dic => [dict],
		)), 
		CompressiveLearning.CGMM_BLESS => PrmSpace([
		 	:X => [WorkflowResult(:X)],
		 	:k => [k],
			# 3 is too much, 2 sometimes crashes
			:λ => 10 .^range(-0.5, stop=2.8, length=gridres_m), 
		], KwArgs(
			:kernel_var => [σ²], 
			:decoder => [:CLOMPR],
			:out_dic => [dict],
		)), 
		CompressiveLearning.EM_GMM => PrmSpace([
		 	:X => [WorkflowResult(:X)],
		 	:k => [k],
	   ])
	]),
	(:R, [ evaluate_GMM => PrmSpace([
		 	:X => [WorkflowResult(:X)],
			:C => [WorkflowResult(:C)],
			:k => [k],
			:dic => [dict],
		], KwArgs( :compare_EM => [false] ))
	])
]
backend_kwargs = Dict()
df = ExperimentsManager.prun(e, backend_kwargs;
							 use_threads = use_threads, BLAS_nthreads = 2)
