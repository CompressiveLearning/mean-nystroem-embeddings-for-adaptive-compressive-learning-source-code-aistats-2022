using Distributed, Base.Threads
@everywhere ENV["DATADEPS_LOAD_PATH"] = fetch(@spawnat 1 ENV["DATADEPS_LOAD_PATH"])
@everywhere using Pkg
@everywhere Pkg.activate(dirname(@__DIR__))
@everywhere using CompressiveLearning
@everywhere using ExperimentsManager
@everywhere using FatDatasets
@everywhere using PyCall
@info "Using $(nprocs()) procs and $(nthreads()) threads."

runlocally()

k = 10
σ² = 5000
gridres_m = 15

e = ExperimentsManager.Exp(; 
						   name = "clustering_fma_mfcc_k$(k)_var$(σ²)", 
						   iterations = 50)
dict = logdict(e)

e.workflow = [
	(:X, [FatDatasets.load_dataset => PrmSpace([
			:name => ["fma_mfcc"]
		]),
	]),
	(:C, [
	  	CKM => PrmSpace([
			 :X => [WorkflowResult(:X)],
			 :k => [k],
		], KwArgs(
			:approximation_type => [:Nyström => KwArgs(
										:Nyström_sampling_type => [
											:uniform,
											:greedy_leverage_scores, 
											# :DPPY_leverage_scores 
										], 
										:linop_type => [
											:dense,
											# :WalshHadamard
										]),  
									:random_features
									],
			:m_factor_eq_reals => 10 .^range(log10(0.8), stop=log10(20), length = gridres_m),
			:kernel_var => [σ²], 
			:kernel_type => [:Gaussian],
			:decoder => [:CLOMPR],
			:out_dic => [dict],
		)), 
		CompressiveLearning.CKM_BLESS => PrmSpace([
		 	:X => [WorkflowResult(:X)],
		 	:k => [k],
			# 3 is too much, 2 sometimes crashes
			:λ => 10 .^range(2.5, stop=-0.5, length=gridres_m), 
		], KwArgs(
			:kernel_var => [σ²], 
			:decoder => [:CLOMPR],
			:out_dic => [dict],
		)), 
	]),
	(:R, [ evaluate_kmeans => PrmSpace([
		 	:X => [WorkflowResult(:X)],
			:C => [WorkflowResult(:C)],
			:dic => [dict],
		], KwArgs( :compute_RSSE => [false] ))
	])
]
backend_kwargs = Dict()
df = ExperimentsManager.prun(e, backend_kwargs;)
