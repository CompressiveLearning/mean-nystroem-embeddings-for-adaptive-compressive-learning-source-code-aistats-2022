using Distributed, Base.Threads
@everywhere using Pkg
@everywhere Pkg.activate(dirname(@__DIR__))
@everywhere using Revise
@everywhere using ExperimentsManager
@everywhere using CompressiveLearning
@everywhere using CIFAR10Features
@everywhere using PyCall # For DPPs
@info "Using $(nprocs()) procs and $(nthreads()) threads."

runlocally()

k = 10 # From ground truth
σ² = 450.0 # From experiments
gridres_m = 15
d = 50
dsf = if d== 50
	CIFAR10Features.features_d50
elseif d == 100
	CIFAR10Features.features_d100
else 
	@error "unknown feature size"
end

e = ExperimentsManager.Exp(; 
						   name = "clustering_cifar10_d50_k$(k)_var$(σ²)_withARI", 
						   iterations = 50)
dict = logdict(e)

e.workflow = [
	(:X, [dsf => PrmSpace([]), ]),
	(:L, [CIFAR10Features.labels => PrmSpace([]), ]),
	(:C, [
		 CKM => PrmSpace([
			 :X => [WorkflowResult(:X)],
			 :k => [k],
		], KwArgs(
			:approximation_type => [:Nyström => KwArgs(
										:Nyström_sampling_type => [
											:uniform,
											:greedy_leverage_scores, 
										], 
										:linop_type => [ :dense, ]),  
									:random_features
									],
			:m_factor_eq_reals => 10 .^range(log10(0.8), stop=log10(20), length = gridres_m),
			:kernel_var => [σ²], 
			:kernel_type => [:Gaussian],
			:decoder => [:CLOMPR],
			:out_dic => [dict],
		)), 
		CompressiveLearning.CKM_BLESS => PrmSpace([
			 :X => [WorkflowResult(:X)],
			 :k => [k],
			:λ => 10 .^range(log10(10), stop=log10(3200), length=gridres_m), 
		], KwArgs(
			:kernel_var => [σ²], 
			:decoder => [:CLOMPR],
			:out_dic => [dict],
		)), 
		CompressiveLearning.kmeans_multitrials => PrmSpace([
			:X => [WorkflowResult(:X)],
			:k => [k],
		], KwArgs(
			:out_dic => [dict],
		))
	]),
	(:R, [ evaluate_kmeans => PrmSpace([
		 	:X => [WorkflowResult(:X)],
			:C => [WorkflowResult(:C)],
			:dic => [dict],
		], KwArgs(
			:compute_RSSE => [true], 
			:compute_ARI => [true], 
			:trueclasses => [WorkflowResult(:L)]
		))
	])
]
backend_kwargs = Dict()
df = ExperimentsManager.prun(e, backend_kwargs;)
