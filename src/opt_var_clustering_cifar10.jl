using Distributed, Base.Threads
@everywhere ENV["DATADEPS_LOAD_PATH"] = fetch(@spawnat 1 ENV["DATADEPS_LOAD_PATH"])
@everywhere using Pkg
@everywhere Pkg.activate(dirname(@__DIR__))
@everywhere using CompressiveLearning
@everywhere using ExperimentsManager
@everywhere using CIFAR10Features

runlocally()

k = 10
mf = 10.0
gridres = 30

e = ExperimentsManager.Exp(; 
						   name = "opt_var_clustering_cifar10_mf$(mf)", 
						   iterations = 30)
dict = logdict(e)

e.workflow = [
	(:X, [CIFAR10Features.features_d50 => PrmSpace([]), ]),
	(:L, [CIFAR10Features.labels => PrmSpace([]), ]),
	(:C, [CKM => PrmSpace([
		 	:X => [WorkflowResult(:X)],
		 	:k => [k],
		], KwArgs(
			:approximation_type => [:Nyström => KwArgs(
										:Nyström_sampling_type => [
											:uniform,
											# :greedy_leverage_scores, 
										]),  
									:random_features
								],
			:m_factor_eq_reals => [mf], 
			# sq. min intra-cluster ≈ 1200
			:kernel_var => 10 .^range(2, stop=4, length = gridres), 
			:kernel_type => [:Gaussian],
			:decoder => [:CLOMPR],
			:out_dic => [dict],
		)) ,
		CompressiveLearning.kmeans_multitrials => PrmSpace([
			:X => [WorkflowResult(:X)],
		 	:k => [k],
	   	], KwArgs(
			:out_dic => [dict],
		))
	]),
	(:R, [ evaluate_kmeans => PrmSpace([
		 	:X => [WorkflowResult(:X)],
			:C => [WorkflowResult(:C)],
			:dic => [dict],
		], KwArgs(
			:compute_RSSE => [true], 
			:trueclasses => [WorkflowResult(:L)]
		))
	])
]
backend_kwargs = Dict()
df = ExperimentsManager.prun(e, backend_kwargs;)
