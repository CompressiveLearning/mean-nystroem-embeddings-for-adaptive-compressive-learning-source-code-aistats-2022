@everywhere using Pkg
@everywhere Pkg.activate(dirname(@__DIR__))
@everywhere using Revise
@everywhere using ExperimentsManager
@everywhere using CompressiveLearning
@everywhere using SpectralMNIST
@everywhere using PyCall 

runlocally()

k = 10
σ²_RF = 0.3
σ²_Nys = 0.095 
gridres_m = 15

e = ExperimentsManager.Exp(; 
						   name = "gmm_smnist_1m_k$(k)_doublevar", 
						   iterations = 50)
dict = logdict(e)

e.workflow = [
	(:X, [SpectralMNIST.centered_mnist_1m => PrmSpace([]), ]),
	(:C, [
		CGMM => PrmSpace([
		 	:X => [WorkflowResult(:X)],
		 	:k => [k],
		], KwArgs(
			:approximation_type => [:Nyström => KwArgs(
										:Nyström_sampling_type => [
											:uniform,
											:greedy_leverage_scores, 
										],
										:kernel_var => [0.095], 
									),  
									:random_features => KwArgs(
										:kernel_var => [σ²_RF], 
									)
									],
			:m_factor_eq_reals => 10 .^range(log10(2), stop=log10(40), length = gridres_m),
			:kernel_type => [:Gaussian],
			:decoder => [:CLOMPR],
			:out_dic => [dict],
		)), 
		CompressiveLearning.CGMM_BLESS => PrmSpace([
			 :X => [WorkflowResult(:X)],
			 :k => [k],
			# 3 is too much, 2 sometimes crashes
			:λ => 10 .^range(-1, 3.3, length=gridres_m), 
		], KwArgs(
			:kernel_var => [σ²_Nys], 
			:decoder => [:CLOMPR],
			:out_dic => [dict],
		)),
		CompressiveLearning.EM_GMM => PrmSpace([
			 :X => [WorkflowResult(:X)],
			 :k => [k],
	   ])
	]),
	(:R, [ evaluate_GMM => PrmSpace([
		 	:X => [WorkflowResult(:X)],
			:C => [WorkflowResult(:C)],
			:k => [k],
			:dic => [dict],
		], KwArgs( :compare_EM => [false] ))
	])
]
backend_kwargs = Dict()
df = ExperimentsManager.prun(e, backend_kwargs;)
