using Distributed, Base.Threads
using Logging
@everywhere using Pkg
@everywhere Pkg.activate(dirname(@__DIR__))
@everywhere using Revise
@everywhere using ExperimentsManager
@everywhere using CompressiveLearning
@everywhere using FatDatasets
@everywhere using PyCall # For DPPs
@info "Using $(nprocs()) procs and $(nthreads()) threads."

runlocally()

d = 30
k = 20
# d = 10
# k = 10
gridres_m = 24
sep = 2.0
n = 20000

σ² = FatDatasets.opt_kvar_from_sep(sep, d, k)

e = ExperimentsManager.Exp(; 
						   name = "gmm_2d_synthetic_k$(k)_d$(d)_n$(n)_sep$(sep)_ran2", 
						   iterations = 50)
dict = logdict(e)

e.workflow = [
	(:X, [GMM_dataset => PrmSpace([
			:k => Int[k], 
			:d => Int[d],
			:n => Int[n],
		], KwArgs(
			:out_dic => [dict],
			:intra_var => [1.0],
			:separation => [sep],
		)),
	]),
	(:C, [CGMM => PrmSpace([
		 	:X => [WorkflowResult(:X)],
		 	:k => [WorkflowPrm(:X, :k)],
		], KwArgs(
			:approximation_type => [
									:Nyström => KwArgs(
										:Nyström_sampling_type => [
											:uniform,
										]),  
									:random_features
									],
			:m_factor_eq_reals => 10 .^range(log10(1.0), stop=log10(10), length = gridres_m),
			:kernel_var => 10 .^range(0, stop=3, length = gridres_m), 
			:kernel_type => [:Gaussian],
			:decoder => [:CLOMPR],
			:out_dic => [dict],
		)) 
	]),
	(:R, [ evaluate_GMM => PrmSpace([
		 	:X => [WorkflowResult(:X)],
			:C => [WorkflowResult(:C)],
			:k => [WorkflowPrm(:X, :k)],
			:dic => [dict],
		], KwArgs( :compute_symKL => [true], :compare_EM => [false] ))
	])
]
backend_kwargs = Dict()
df = ExperimentsManager.prun(e, backend_kwargs; 
							 parallelization_method = :pmap, # For GMM/Bless no threads
							 logging_level = Logging.Debug, 
							 BLAS_nthreads = 1, 
							 log_to_files = true, 
							 continue_on_errors = true, 
							 )

