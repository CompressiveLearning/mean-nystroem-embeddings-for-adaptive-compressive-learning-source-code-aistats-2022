using Distributed, Base.Threads
@everywhere using Pkg
@everywhere Pkg.activate(dirname(@__DIR__))
@everywhere using Revise
@everywhere using ExperimentsManager
@everywhere using CompressiveLearning
@everywhere using SpectralMNIST
@everywhere using PyCall # For DPPs
@info "Using $(nprocs()) procs and $(nthreads()) threads."

runlocally()

k = 10
σ² = 0.3 # From thesis exps
gridres_m = 15

e = ExperimentsManager.Exp(; 
						   name = "clustering_smnist_k$(k)_var$(σ²)", 
						   iterations = 50)
dict = logdict(e)

e.workflow = [
	(:X, [SpectralMNIST.centered_mnist_70k => PrmSpace([]), ]),
	(:C, [
		 CKM => PrmSpace([
			 :X => [WorkflowResult(:X)],
			 :k => [k],
		], KwArgs(
			:approximation_type => [:Nyström => KwArgs(
										:Nyström_sampling_type => [
											# :uniform,
											:greedy_leverage_scores, 
										], 
										:linop_type => [
											:dense,
											# :WalshHadamard
										]),  
									# :random_features
									],
			:m_factor_eq_reals => 10 .^range(log10(0.8), stop=log10(20), length = gridres_m),
			:kernel_var => [σ²], 
			:kernel_type => [:Gaussian],
			:decoder => [:CLOMPR],
			:out_dic => [dict],
		)), 
		CompressiveLearning.CKM_BLESS => PrmSpace([
		 	:X => [WorkflowResult(:X)],
		 	:k => [k],
			:λ => 10 .^range(log10(10), stop=log10(3200), length=gridres_m), 
		], KwArgs(
			:kernel_var => [σ²], 
			:decoder => [:CLOMPR],
			:out_dic => [dict],
		)), 
		CompressiveLearning.kmeans_multitrials => PrmSpace([
			:X => [WorkflowResult(:X)],
		 	:k => [k],
	   	], KwArgs(
			:out_dic => [dict],
		))
	]),
	(:R, [ evaluate_kmeans => PrmSpace([
		 	:X => [WorkflowResult(:X)],
			:C => [WorkflowResult(:C)],
			:dic => [dict],
		], KwArgs( :compute_RSSE => [true] ))
	])
]
backend_kwargs = Dict()
df = ExperimentsManager.prun(e, backend_kwargs;)
