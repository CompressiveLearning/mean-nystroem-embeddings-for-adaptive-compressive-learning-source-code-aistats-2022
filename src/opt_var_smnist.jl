using Distributed, Base.Threads
@everywhere ENV["DATADEPS_LOAD_PATH"] = fetch(@spawnat 1 ENV["DATADEPS_LOAD_PATH"])
@everywhere using Pkg
@everywhere Pkg.activate(dirname(@__DIR__))
@everywhere using CompressiveLearning
@everywhere using ExperimentsManager
@everywhere using SpectralMNIST
@everywhere using PyCall
@info "Using $(nprocs()) procs and $(nthreads()) threads."

runlocally()

k = 10
gridres_m = 40
mf = 30.0

e = ExperimentsManager.Exp(; 
						   name = "opt_var_gmm_smnist_1m_mf$(mf)", 
						   iterations = 30)
dict = logdict(e)

e.workflow = [
	(:X, [SpectralMNIST.centered_mnist_1m => PrmSpace([]), ]),
	(:C, [CGMM => PrmSpace([
		 	:X => [WorkflowResult(:X)],
		 	:k => [k],
		], KwArgs(
			:approximation_type => [:Nyström => KwArgs(
										:Nyström_sampling_type => [
											:uniform,
											:greedy_leverage_scores, 
										]),  
									:random_features
								],
			:m_factor_eq_reals => [mf], 
			#σ² = 0.3 for CKM
			:kernel_var => 10 .^range(-2, stop=1, length = gridres_m), 
			:kernel_type => [:Gaussian],
			:decoder => [:CLOMPR],
			:out_dic => [dict],
		)) ,
		CompressiveLearning.EM_GMM => PrmSpace([
			 :X => [WorkflowResult(:X)],
			 :k => [k],
	   ])

	]),
	(:R, [ evaluate_GMM => PrmSpace([
		 	:X => [WorkflowResult(:X)],
			:C => [WorkflowResult(:C)],
			:k => [k],
			:dic => [dict],
		], KwArgs( :compare_EM => [false] ))
	])
]
backend_kwargs = Dict()
df = ExperimentsManager.prun(e, backend_kwargs;)
