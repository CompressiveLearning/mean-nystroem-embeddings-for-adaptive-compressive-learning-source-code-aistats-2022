@everywhere using Pkg
@everywhere Pkg.activate(dirname(@__DIR__))
@everywhere using Revise
@everywhere using ExperimentsManager
@everywhere using CompressiveLearning
@everywhere using FatDatasets
@everywhere using PyCall # For DPPs / LS

runlocally()

function main_gmm_synthetic(d, k)

	@info "Running main_gmm_synthetic with d=$d, k=$k"

	gridres_m = 15
	sep = 2.0
	n = Int(10^5)

	σ² = FatDatasets.opt_kvar_from_sep(sep, d, k)
	# 20/30: ≈81
	# 10/10: ≈23

	λmin, λmax, σ²_RF, σ²_Nys = if d == 2
		1e2, 1e3, σ², σ²
	elseif d == 10
		10, 200, σ², σ²	# Obtained manually
	elseif d == 30
		0.8, 30, σ², 20.0
	else
		@error "Unknown λmin/λmax."
	end

	e = ExperimentsManager.Exp(; 
							   name = "gmm_synthetic_k$(k)_d$(d)_sep$(sep)_n$(n)", 
							   iterations = 50)
	dict = logdict(e)

	e.workflow = [
		(:X, [GMM_dataset => PrmSpace([
				:k => Int[k], 
				:d => Int[d],
				:n => Int[n],
			], KwArgs(
				:out_dic => [dict],
				:intra_var => [1.0],
				:separation => [sep],
			)),
		]),
		(:C, [
			CGMM => PrmSpace([
				 :X => [WorkflowResult(:X)],
				 :k => [WorkflowPrm(:X, :k)],
			], KwArgs(
				:approximation_type => [
										:Nyström => KwArgs(
											:Nyström_sampling_type => [
												:uniform,
												:greedy_leverage_scores, 
											],
											:kernel_var => [σ²_Nys], 
										),  
										:random_features => KwArgs(
											:kernel_var => [σ²_RF], 
										),
				],
				:m_factor_eq_reals => 10 .^range(log10(1.0), stop=log10(20), length = gridres_m),
				:kernel_type => [:Gaussian],
				:decoder => [:CLOMPR],
				:out_dic => [dict],
			)),
			CGMM_BLESS => PrmSpace([
				:X => [WorkflowResult(:X)],
				:k => [WorkflowPrm(:X, :k)],
				:λ => 10 .^range(log10(λmax), stop=log10(λmin), length=gridres_m), 
			], KwArgs(
				:kernel_var => [σ²_Nys],
				:decoder => [:CLOMPR],
				:out_dic => [dict],
			)), 
			CompressiveLearning.EM_GMM => PrmSpace([
				 :X => [WorkflowResult(:X)],
				 :k => [k],
			]),
		]),
		(:R, [ evaluate_GMM => PrmSpace([
				:X => [WorkflowResult(:X)],
				:C => [WorkflowResult(:C)],
				:k => [WorkflowPrm(:X, :k)],
				:dic => [dict],
			], KwArgs( :compute_symKL => [true],
					   :compare_EM => [false] ))
		])
	]
	backend_kwargs = Dict()
	df = ExperimentsManager.prun(e, backend_kwargs;)

end

