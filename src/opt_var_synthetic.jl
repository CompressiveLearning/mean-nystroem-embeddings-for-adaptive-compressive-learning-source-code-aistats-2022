using Distributed, Base.Threads
@everywhere ENV["DATADEPS_LOAD_PATH"] = fetch(@spawnat 1 ENV["DATADEPS_LOAD_PATH"])
@everywhere using Pkg
@everywhere Pkg.activate(dirname(@__DIR__))
@everywhere using CompressiveLearning
@everywhere using ExperimentsManager
@everywhere using FatDatasets
@everywhere using PyCall
@info "Using $(nprocs()) procs and $(nthreads()) threads."

runlocally()

# d = 10
# k = 10
d = 30
k = 20

n = 10^5 
sep = 2.0
gridres_m = 30


e = ExperimentsManager.Exp(; 
						   name = "opt_var_gmm_synth_k$(k)_d$(d)_mf10_n$(n)", 
						   iterations = 30)
dict = logdict(e)

e.workflow = [
	(:X, [GMM_dataset => PrmSpace([
			:k => Int[k], 
			:d => Int[d],
			:n => Int[n],
		], KwArgs(
			:out_dic => [dict],
			:intra_var => [1.0],
			:separation => [sep],
		)),
	]),
	(:C, [
		CGMM => PrmSpace([
		 	:X => [WorkflowResult(:X)],
		 	:k => [k],
		], KwArgs(
			:approximation_type => [
									:Nyström => KwArgs(
										:Nyström_sampling_type => [
											:uniform,
											:greedy_leverage_scores, 
										]),  
									:random_features
								],
			:m_factor_eq_reals => [10.0], 
			# σ² = FatDatasets.opt_kvar_from_sep(sep, d, k) # 80 for 2.0/30/20
			:kernel_var => 10 .^range(0, stop=3, length = gridres_m), 
			:kernel_type => [:Gaussian],
			:decoder => [:CLOMPR],
			:out_dic => [dict],
		)),
		CompressiveLearning.EM_GMM => PrmSpace([
		 	:X => [WorkflowResult(:X)],
		 	:k => [k],
	    ]),

	]),
	(:R, [ evaluate_GMM => PrmSpace([
		 	:X => [WorkflowResult(:X)],
			:C => [WorkflowResult(:C)],
			:k => [k],
			:dic => [dict],
		], KwArgs(
			:compare_EM => [false], 
			# :compute_symKL => [true], 
		))
	])
]
backend_kwargs = Dict()
df = ExperimentsManager.prun(e, backend_kwargs;)
