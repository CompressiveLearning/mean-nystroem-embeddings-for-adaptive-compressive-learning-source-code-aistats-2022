using Distributed, Base.Threads
@everywhere using Pkg
@everywhere Pkg.activate(dirname(@__DIR__))
@everywhere using Revise
@everywhere using ExperimentsManager
@everywhere using CompressiveLearning
@everywhere using FatDatasets
@everywhere using PyCall # For DPPs/Bless
@info "Using $(nprocs()) procs and $(nthreads()) threads."

runlocally()

function main_clustering_synthetic(d,k)

	gridres_m = 16
	sep = 2.0
	n = Int(1e4)

	λmin, λmax = if d == 2
		1e2, 1e3	# For 2/5: stop at λ=5
	elseif d == 10
		10, 200	# Obtained manually
	elseif d == 30
		0.8, 30	# To test
	else
		@error "Unknown λmin/λmax."
	end

	σ² = FatDatasets.opt_kvar_from_sep(sep, d, k)

	e = ExperimentsManager.Exp(; 
							   name = "clustering_synthetic_k$(k)_d$(d)_n$(n)_sep$(sep)", 
							   iterations = 50)
	dict = logdict(e)

	e.workflow = [
		(:X, [GMM_dataset => PrmSpace([
				:k => Int[k], 
				:d => Int[d],
				:n => Int[n],
			], KwArgs(
				:out_dic => [dict],
				:intra_var => [1.0],
				:separation => [sep],
				# :inter_intra_var_ratio => [iivr],
			)),
		]),
		(:S, [dataset_stats => PrmSpace([
				:X => [WorkflowResult(:X)],
				:out_dic => [dict],
			]),
		]),
		(:C, [CKM => PrmSpace([
				:X => [WorkflowResult(:X)],
				:k => [WorkflowPrm(:X, :k)],
			], KwArgs(
				:approximation_type => [:Nyström => KwArgs(
											:Nyström_sampling_type => [
												:uniform,
												:greedy_leverage_scores, 
												# :DPPYLeverageScores 
											]),  
										:random_features
										],
				:m_factor_eq_reals => 10 .^range(log10(1.0), stop=log10(10), length = gridres_m),
				:kernel_var => [σ²], 
				:kernel_type => [:Gaussian],
				:decoder => [:CLOMPR],
				:out_dic => [dict],
			)) ,
			CompressiveLearning.CKM_BLESS => PrmSpace([
				:X => [WorkflowResult(:X)],
				:k => [WorkflowPrm(:X, :k)],
				:λ => 10 .^range(log10(λmax), stop=log10(λmin), length=gridres_m), 
			], KwArgs(
				:kernel_var => [σ²], 
				:decoder => [:CLOMPR],
				:out_dic => [dict],
			)), 
			CompressiveLearning.kmeans_multitrials => PrmSpace([
				 :X => [WorkflowResult(:X)],
				:k => [WorkflowPrm(:X, :k)],
			], KwArgs(
				:out_dic => [dict],
			))
		]),
		(:R, [ evaluate_kmeans => PrmSpace([
				:X => [WorkflowResult(:X)],
				:C => [WorkflowResult(:C)],
				:dic => [dict],
			], KwArgs( :compute_RSSE => [true] ))
		])
	]
	backend_kwargs = Dict()
	df = ExperimentsManager.prun(e, backend_kwargs; 
								 parallelization_method = :pmap,
								 log_to_files = true, 
								 continue_on_errors = true, 
								 # continue_on_errors = false, 
								 BLAS_nthreads = 1, 
								 )
end
